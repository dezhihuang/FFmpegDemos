参考：
[https://blog.csdn.net/rootusers/article/details/43560913](https://blog.csdn.net/rootusers/article/details/43560913)
[https://blog.csdn.net/rootusers/article/details/43563133](https://blog.csdn.net/rootusers/article/details/43563133)

```c
//
//解码H264数据，并保存为yuv数据文件
//
//FFmpeg 4.0
//
//Windows
//

#include <stdio.h>


#define __STDC_CONSTANT_MACROS

extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/avutil.h"  
};


#pragma comment (lib, "avformat.lib")
#pragma comment (lib, "avutil.lib")
#pragma comment (lib, "avcodec.lib")
#pragma comment (lib, "swscale.lib")


int FindStartCode (unsigned char *Buf, int zeros_in_startcode)  
{  
    int info;  
    int i;  

    info = 1;  
    for (i = 0; i < zeros_in_startcode; i++) { 
        if(Buf[i] != 0) {  
            info = 0;
        }
    }

    if(Buf[i] != 1) { 
        info = 0; 
    }

    return info;  
}  


int getNextNal(FILE* inpf, unsigned char* Buf)//打开的文件句柄，和将读取的字节存储到Buf中  
{ 
    int pos = 0;  
    int StartCodeFound = 0;  
    int info2 = 0;  
    int info3 = 0;  
    int i = 0;  
    while (!feof(inpf) && (Buf[pos++] = fgetc(inpf)) == 0);//跳过NAL头  

    /*通过查找下一次的NAL头来计算本次的NAL的长度，根据长度来读取NAL数据包*/  
    while (!StartCodeFound)  
    {  
        if (feof (inpf))  
        {  
            return pos-1;  
        }  
        Buf[pos++] = fgetc (inpf);    
        info3 = FindStartCode(&Buf[pos-4], 3);  
        if(info3 != 1)  {
            info2 = FindStartCode(&Buf[pos-3], 2); 
        }
        StartCodeFound = (info2 == 1 || info3 == 1);  
    } 
    if (info2) {
        fseek (inpf, -3, SEEK_CUR); 
        return pos - 3;//返回的就是一个NAL的长度 
    } else {
        fseek (inpf, -4, SEEK_CUR); 
        return pos - 4;//返回的就是一个NAL的长度 
    } 
}  

void decode(AVCodecContext *pCodecCtx, AVPacket *packet, AVFrame *pFrame, FILE *out_file)
{
    int ret = 0;
    int i = 0;
    static int cnt = 0;

    // 将原始包数据作为输入提供给解码器
    ret = avcodec_send_packet(pCodecCtx, packet);
    if (ret < 0) {
        fprintf(stderr, "Error sending a packet for decoding\n");
        return;
    }

    while (ret >= 0) {
        // 从解码器返回解码后输出数据
        ret = avcodec_receive_frame(pCodecCtx, pFrame);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
            break;
        } else if (ret < 0) {
            fprintf(stderr, "Error during decoding\n");
            break;
        }


        cnt++;


        /*从二维空间中提取解码后的图像*/  
        for(i=0; i<pCodecCtx->height; i++)  
            fwrite(pFrame->data[0] + i * pFrame->linesize[0], 1, pCodecCtx->width, out_file);  
        for(i=0; i<pCodecCtx->height/2; i++)  
            fwrite(pFrame->data[1] + i * pFrame->linesize[1], 1, pCodecCtx->width/2, out_file); 
        for(i=0; i<pCodecCtx->height/2; i++)  
            fwrite(pFrame->data[2] + i * pFrame->linesize[2], 1, pCodecCtx->width/2, out_file); 

        printf("Succeed to decode %d frame!\r", cnt);
    }
}


int main(){  
    FILE * inp_file;  
    FILE * out_file;  

    int i = 0;  
    int nalLen = 0;     /*NAL 长度*/  
    unsigned char* Buf; /*H.264码流*/    
    int cnt=0;  

    AVCodec        *pCodec;     /* 编解码CODEC*/  
    AVCodecContext *pCodecCtx;  /* 编解码CODEC context*/  
    AVFrame        *pFrame;     /* 解码后的图像*/     

    /*输入的文件*/  
    inp_file = fopen("D:\\Workspace\\7ffmpeg\\demo\\out\\out.h264", "rb");  
    if (inp_file == NULL) {
        return -1;
    }

    /*输出的文件*/  
    out_file = fopen("test.yuv", "wb");  
    if (out_file == NULL) {
        return -1;
    }

 
    /*分配内存，并初始化为0*/  
    Buf = (unsigned char*)calloc ( 500*1024, sizeof(char));  
 

    /*查找 H264 CODEC*/  
    pCodec = avcodec_find_decoder(AV_CODEC_ID_H264);  
    if (pCodec == NULL) {
        return -1;
    } 

    /*初始化CODEC的默认参数*/  
    pCodecCtx = avcodec_alloc_context3(NULL); 
    if (pCodecCtx == NULL) {
        return -1;
    }  

    //pCodecCtx->width = 480;
    //pCodecCtx->height = 800;
    //pCodecCtx->pix_fmt = AV_PIX_FMT_YUV420P;


    //打开解码器
    if(avcodec_open2(pCodecCtx, pCodec, NULL)<0){
        printf("Could not open codec.\n");
        return -1;
    }


    /*为AVFrame申请空间，并清零*/  
    pFrame = av_frame_alloc();
    if (pFrame==NULL) {
        printf("av_frame_alloc() failed!\n");
        return -1;
    }


    /*循环解码*/  
    while(!feof(inp_file))  {  
        /*从码流中获得一个NAL包*/  
        nalLen = getNextNal(inp_file, Buf);  

        AVPacket packet = {0};
        packet.data = Buf;
        packet.size = nalLen;

        decode(pCodecCtx, &packet, pFrame, out_file);

        av_packet_unref(&packet);

    } 
    // flush the decoder 
    decode(pCodecCtx, NULL, pFrame, out_file);
    printf("\n\n");

    /*关闭文件*/  
    if(inp_file) {
        fclose(inp_file);
        inp_file = NULL;
    }  
    if(out_file) {
        fclose(out_file);
        out_file = NULL;
    }

    /*3. 关闭CODEC，释放资源,调用decode_end本地函数*/  
    if(pCodecCtx) {  
        avcodec_close(pCodecCtx);   
        av_free(pCodecCtx);  
        pCodecCtx = NULL;  
    }   
    /*释放AVFrame空间*/  
    if(pFrame) {  
        av_free(pFrame);  
        pFrame = NULL;  
    }  
    /*释放内存*/  
    if(Buf) {  
        free(Buf);  
        Buf = NULL;  
    }      

    return 0;  
} 
```