
参考：[https://blog.csdn.net/u012587637/article/details/79653807](https://blog.csdn.net/u012587637/article/details/79653807)

注意：解码输出的音频格式不能是AV_SAMPLE_FMT_S16P，否则程序运行崩溃。原始音频采样率是48000HZ如果改为44100HZ会出现杂音。

```c

#include <stdio.h>

//
// 把一个视频文件解码成为单独的音频 PCM 文件和视频 YUV 文件
//


#define __STDC_CONSTANT_MACROS

extern "C" {
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"
#include "libavutil/pixdesc.h"
#include "libavutil/imgutils.h"  
}

#pragma comment (lib, "avformat.lib")
#pragma comment (lib, "avutil.lib")
#pragma comment (lib, "avcodec.lib")
#pragma comment (lib, "swscale.lib")
#pragma comment (lib, "swresample.lib")

struct AudioParam {
    enum AVSampleFormat sample_fmt;      // sample format
    int                 sample_rate;     // samples per second
    uint64_t            channel_layout;  // Audio channel layout
    int                 channels;        // number of audio channels
};


void decode(AVCodecContext *pCodecCtx, AVPacket *packet, AVFrame *pFrame, AVFrame *pFrameYUV, struct SwsContext *img_convert_ctx, FILE *fp_yuv) 
{
    int ret = 0;
    static int frameCnt = 0;

    // 将原始包数据作为输入提供给解码器
    ret = avcodec_send_packet(pCodecCtx, packet);
    if (ret < 0) {
        fprintf(stderr, "Error sending a packet for decoding\n");
        return;
    }

    while (ret >= 0) {
        // 从解码器返回解码后输出数据
        ret = avcodec_receive_frame(pCodecCtx, pFrame);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
            break;
        } else if (ret < 0) {
            fprintf(stderr, "Error during decoding\n");
            break;
        }

        sws_scale(img_convert_ctx, (const uint8_t* const*)pFrame->data, pFrame->linesize, 0, pCodecCtx->height, 
            pFrameYUV->data, pFrameYUV->linesize);

        int y_size=pCodecCtx->width*pCodecCtx->height;  
        fwrite(pFrameYUV->data[0],1,y_size,fp_yuv);    //Y 
        fwrite(pFrameYUV->data[1],1,y_size/4,fp_yuv);  //U
        fwrite(pFrameYUV->data[2],1,y_size/4,fp_yuv);  //V

        frameCnt++;
        printf("Succeed to decode %d frame!\r", frameCnt);
    }

}


void decodeAudio(AVCodecContext *dec_ctx, AVPacket *pkt, AVFrame *frame,FILE *outfile, struct SwrContext *swrCtx, struct AudioParam outAudioParam)
{
    /* send the packet with the compressed data to the decoder */
    int ret = avcodec_send_packet(dec_ctx, pkt);
    if (ret < 0) {
        fprintf(stderr, "Error submitting the packet to the decoder\n");
        exit(1);
    }

    /* read all the output frames (in general there may be any number of them */
    while (ret >= 0) {
        ret = avcodec_receive_frame(dec_ctx, frame);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
            return;
        else if (ret < 0) {
            fprintf(stderr, "Error during decoding\n");
            exit(1);
        }

        //获取sample的size
        int out_buffer_size = av_samples_get_buffer_size(NULL, outAudioParam.channels, frame->nb_samples, outAudioParam.sample_fmt, 1);

        uint8_t *out_buffer = (uint8_t *)av_malloc(out_buffer_size);

        swr_convert(swrCtx, &out_buffer, out_buffer_size, (const uint8_t**)frame->data, frame->nb_samples);
        
        fwrite(out_buffer, 1, out_buffer_size, outfile);

        av_free(out_buffer);
    }
}


int main()
{
    AVFormatContext *pFormatCtx;

    AVCodecContext  *pVideoCodecCtx;
    AVCodec         *pVideoCodec;

    AVCodecContext  *pAudioCodecCtx;
    AVCodec         *pAudioCodec;
    
    AVFrame         *pFrame;
    AVFrame         *pFrameYUV;

    uint8_t *out_buffer = NULL;

    struct SwrContext *pSwrContext;
    struct SwsContext *img_convert_ctx;
    AVPacket        *packet;

    unsigned int i = 0;
    int videoIndex = -1;
    int audioIndex = -1;
    char mp4FilePath[] = "D:\\Workspace\\7ffmpeg\\ffmpegExamples\\data\\video_audio_480x800.mp4";

    FILE *fp_yuv  = fopen("output.yuv","wb+");
    if (fp_yuv == NULL) {
        printf("output.yuv create or open failed!\n");
        return -1;
    }
    FILE *fp_pcm  = fopen("output.pcm","wb+");
    if (fp_pcm == NULL) {
        printf("output.pcm create or open failed!\n");
        return -1;
    }


    //初始化一个AVFormatContext
    pFormatCtx = avformat_alloc_context();
    if (pFormatCtx == NULL) {
        return -1;
    }

    //打开输入的视频文件
    if(avformat_open_input(&pFormatCtx, mp4FilePath, NULL, NULL) != 0){
        printf("Couldn't open input stream.\n");
        return -1;
    }

    //获取视频文件信息
    if(avformat_find_stream_info(pFormatCtx, NULL) < 0){
        printf("Couldn't find stream information.\n");
        return -1;
    }

    //输出视频文件信息
    printf("--------------- File Information ----------------\n");
    av_dump_format(pFormatCtx, 0, mp4FilePath, 0);
    printf("-------------------------------------------------\n");

    //查找视频流
    for (i=0; i<pFormatCtx->nb_streams; i++) {
        if (pFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
            videoIndex = i;
            break;
        }
    }
    if (videoIndex == -1) {
        printf("Couldn't find a video stream.\n");  
        return -1;
    }


    //查找音频流
    for (i=0; i<pFormatCtx->nb_streams; i++) {
        if (pFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO) {
            audioIndex = i;
            break;
        }
    }
    if (audioIndex == -1) {
        printf("Couldn't find a audio stream.\n"); 
        return -1;
    }



    //打开视频流解码器：
    pVideoCodecCtx = avcodec_alloc_context3(NULL);
    if (pVideoCodecCtx == NULL) {
        return -1;
    }
    avcodec_parameters_to_context(pVideoCodecCtx, pFormatCtx->streams[videoIndex]->codecpar);
    pVideoCodec = avcodec_find_decoder(pVideoCodecCtx->codec_id);
    if (pVideoCodec == NULL) {
        printf("Codec not found.\n"); 
        return -1;
    }
    //打开解码器
    if (avcodec_open2(pVideoCodecCtx, pVideoCodec, NULL) < 0) {
        printf("Could not open codec.\n"); 
        return -1;
    }


    //打开音频流解码器：
    pAudioCodecCtx = avcodec_alloc_context3(NULL);
    if (pAudioCodecCtx == NULL) {
        return -1;
    }
    avcodec_parameters_to_context(pAudioCodecCtx, pFormatCtx->streams[audioIndex]->codecpar);
    pAudioCodec= avcodec_find_decoder(pAudioCodecCtx->codec_id);
    if (pAudioCodec == NULL) {
        printf("Codec not found.\n"); 
        return -1;
    }
    //打开解码器
    if (avcodec_open2(pAudioCodecCtx, pAudioCodec, NULL) < 0) {
        printf("Could not open codec.\n"); 
        return -1;
    }
    

    struct AudioParam in_audioParm = {  pAudioCodecCtx->sample_fmt,
                                        pAudioCodecCtx->sample_rate,
                                        pAudioCodecCtx->channel_layout,
                                        pAudioCodecCtx->channels
                                      };

    struct AudioParam out_audioParm = { AV_SAMPLE_FMT_S16,  //不能使用AV_SAMPLE_FMT_S16P，否则程序运行崩溃
                                        48000,              //44100;
                                        AV_CH_LAYOUT_STEREO,
                                        av_get_channel_layout_nb_channels(AV_CH_LAYOUT_STEREO)
                                      };

    pSwrContext = swr_alloc();
    swr_alloc_set_opts(pSwrContext, out_audioParm.channel_layout, out_audioParm.sample_fmt, out_audioParm.sample_rate,
        in_audioParm.channel_layout, in_audioParm.sample_fmt, in_audioParm.sample_rate, 0, NULL);

    swr_init(pSwrContext);
    
    
    pFrame    = av_frame_alloc();
    pFrameYUV = av_frame_alloc();
    if (pFrame==NULL || pFrameYUV==NULL) {
        printf("av_frame_alloc() failed!\n");
        return -1;
    }

    out_buffer=(uint8_t *)av_malloc(av_image_get_buffer_size(AV_PIX_FMT_YUV420P, pVideoCodecCtx->width, pVideoCodecCtx->height, 1));
    av_image_fill_arrays(pFrameYUV->data, pFrameYUV->linesize, out_buffer, AV_PIX_FMT_YUV420P, pVideoCodecCtx->width, pVideoCodecCtx->height, 1);

    img_convert_ctx = sws_getContext(pVideoCodecCtx->width, pVideoCodecCtx->height, pVideoCodecCtx->pix_fmt, 
        pVideoCodecCtx->width, pVideoCodecCtx->height, AV_PIX_FMT_YUV420P, SWS_BICUBIC, NULL, NULL, NULL); 

    packet = (AVPacket *)av_malloc(sizeof(AVPacket));
    if (packet == NULL) {
        printf("AVPacket malloc failed!\n");
        return -1;
    }

    while(true) {
        //读取一帧压缩数据
        if (av_read_frame(pFormatCtx, packet) < 0) {
            break;
        }

        if(packet->stream_index == videoIndex)
        {
            //解码视频为yuv数据并保存到文件
            decode(pVideoCodecCtx, packet, pFrame, pFrameYUV, img_convert_ctx, fp_yuv);
        } 
        else if (packet->stream_index == audioIndex) 
        {
            //解码音频为pcm数据并保存到文件
            decodeAudio(pAudioCodecCtx, packet, pFrame, fp_pcm, pSwrContext, out_audioParm);
        }

        av_packet_unref(packet);
    }

    // flush the decoder 
    decode(pVideoCodecCtx, NULL, pFrame, pFrameYUV, img_convert_ctx, fp_yuv);
    decodeAudio(pAudioCodecCtx, NULL, pFrame, fp_pcm, pSwrContext, out_audioParm);

    printf("\n");


    //关闭文件以及释放内存
    fclose(fp_yuv);
    fclose(fp_pcm);

    sws_freeContext(img_convert_ctx);
    swr_free(&pSwrContext);
    av_frame_free(&pFrameYUV);
    av_frame_free(&pFrame);
    avcodec_close(pVideoCodecCtx);
    avcodec_close(pAudioCodecCtx);
    avformat_close_input(&pFormatCtx);

    return 0;
}


```