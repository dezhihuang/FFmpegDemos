参考：[https://blog.csdn.net/king1425/article/details/71160339](https://blog.csdn.net/king1425/article/details/71160339)

```c
//
//mp4格式解码为h264和yuv数据
//
//FFmpeg 4.0
//
//Windows
//

#include <stdio.h>


#define __STDC_CONSTANT_MACROS

extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/imgutils.h"  
};


#pragma comment (lib, "avformat.lib")
#pragma comment (lib, "avutil.lib")
#pragma comment (lib, "avcodec.lib")
#pragma comment (lib, "swscale.lib")


void decode(AVCodecContext *pCodecCtx, AVPacket *packet, AVFrame *pFrame, AVFrame *pFrameYUV, struct SwsContext *img_convert_ctx, FILE *fp_yuv) 
{
    int ret = 0;
    static int frameCnt = 0;

    // 将原始包数据作为输入提供给解码器
    ret = avcodec_send_packet(pCodecCtx, packet);
    if (ret < 0) {
        fprintf(stderr, "Error sending a packet for decoding\n");
        return;
    }

    while (ret >= 0) {
        // 从解码器返回解码后输出数据
        ret = avcodec_receive_frame(pCodecCtx, pFrame);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
            break;
        } else if (ret < 0) {
            fprintf(stderr, "Error during decoding\n");
            break;
        }

        sws_scale(img_convert_ctx, (const uint8_t* const*)pFrame->data, pFrame->linesize, 0, pCodecCtx->height, 
            pFrameYUV->data, pFrameYUV->linesize);

        int y_size=pCodecCtx->width*pCodecCtx->height;  
        fwrite(pFrameYUV->data[0],1,y_size,fp_yuv);    //Y 
        fwrite(pFrameYUV->data[1],1,y_size/4,fp_yuv);  //U
        fwrite(pFrameYUV->data[2],1,y_size/4,fp_yuv);  //V

        frameCnt++;
        printf("Succeed to decode %d frame!\r", frameCnt);
    }

}


int main(int argc, char* argv[])
{
    AVFormatContext *pFormatCtx;
    AVCodecContext  *pCodecCtx;
    AVCodec         *pCodec;
    AVPacket        *packet;
    AVFrame         *pFrame;
    AVFrame         *pFrameYUV;
    struct SwsContext *img_convert_ctx;

    unsigned int i      = 0; 
    int videoindex      = -1;
    uint8_t *out_buffer = NULL;
    int ret             = 0;
    

    char filepath[]="D:\\Workspace\\7ffmpeg\\demo\\out\\video.mp4";

    FILE *fp_yuv  = fopen("output.yuv","wb+");
    if (fp_yuv == NULL) {
        printf("output.yuv create or open failed!\n");
        return -1;
    }
    FILE *fp_h264 = fopen("output.h264","wb+");
    if (fp_h264 == NULL) {
        printf("output.h264 create or open failed!\n");
        return -1;
    }

    //初始化一个AVFormatContext
    pFormatCtx = avformat_alloc_context();
    if (pFormatCtx == NULL) {
        return -1;
    }

    //打开输入的视频文件
    if(avformat_open_input(&pFormatCtx,filepath,NULL,NULL) != 0){
        printf("Couldn't open input stream.\n");
        return -1;
    }

    //获取视频文件信息
    if(avformat_find_stream_info(pFormatCtx, NULL) < 0){
        printf("Couldn't find stream information.\n");
        return -1;
    }

    //输出视频文件信息
    printf("--------------- File Information ----------------\n");
    av_dump_format(pFormatCtx, 0, filepath, 0);
    printf("-------------------------------------------------\n");

    //查找视频流索引
    for(i=0; i<pFormatCtx->nb_streams; i++) {
        if(pFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO){
            videoindex = i;
            break;
        }
    }
    if(videoindex == -1){
        printf("Didn't find a video stream.\n");
        return -1;
    }

    //分配AVCodecContext并将其字段设置为默认值
    pCodecCtx = avcodec_alloc_context3(NULL);
    if (pCodecCtx == NULL) {
        return -1;
    }
    avcodec_parameters_to_context(pCodecCtx, pFormatCtx->streams[videoindex]->codecpar);

    //查找解码器
    pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
    if(pCodec==NULL){
        printf("Codec not found.\n");
        return -1;
    }

    //打开解码器
    if(avcodec_open2(pCodecCtx, pCodec,NULL)<0){
        printf("Could not open codec.\n");
        return -1;
    }

    pFrame    = av_frame_alloc();
    pFrameYUV = av_frame_alloc();
    if (pFrame==NULL || pFrameYUV==NULL) {
        printf("av_frame_alloc() failed!\n");
        return -1;
    }

    out_buffer=(uint8_t *)av_malloc(av_image_get_buffer_size(AV_PIX_FMT_YUV420P, pCodecCtx->width, pCodecCtx->height, 1));
    av_image_fill_arrays(pFrameYUV->data, pFrameYUV->linesize, out_buffer, AV_PIX_FMT_YUV420P, pCodecCtx->width, pCodecCtx->height, 1);

    img_convert_ctx = sws_getContext(pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt, 
        pCodecCtx->width, pCodecCtx->height, AV_PIX_FMT_YUV420P, SWS_BICUBIC, NULL, NULL, NULL); 

    packet = (AVPacket *)av_malloc(sizeof(AVPacket));
    if (packet == NULL) {
        printf("AVPacket malloc failed!\n");
        return -1;
    }

    while(true) {
        //读取一帧压缩数据
        if (av_read_frame(pFormatCtx, packet) < 0) {
            break;
        }

        if(packet->stream_index == videoindex){

            //把H264数据写入fp_h264文件
            fwrite(packet->data,1,packet->size,fp_h264); 

            //解码为yuv数据并保存到文件
            decode(pCodecCtx, packet, pFrame, pFrameYUV, img_convert_ctx, fp_yuv);
        }
        av_packet_unref(packet);
    }
    printf("\n");

    // flush the decoder 
    decode(pCodecCtx, NULL, pFrame, pFrameYUV, img_convert_ctx, fp_yuv);

    //关闭文件以及释放内存
    fclose(fp_yuv);
    fclose(fp_h264);

    sws_freeContext(img_convert_ctx);
    av_frame_free(&pFrameYUV);
    av_frame_free(&pFrame);
    avcodec_close(pCodecCtx);
    avformat_close_input(&pFormatCtx);

    return 0;
}
```


## 注意

当av_read_frame()循环退出的时候，实际上解码器中可能还包含剩余的几帧数据。 
因此需要通过“flush the decoder”将这几帧数据输出。“flush the decoder”功能简而言之即直接调用avcodec_receive_frame()获得AVFrame，而不再向解码器传递AVPacket。