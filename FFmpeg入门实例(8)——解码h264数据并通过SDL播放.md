
```c
#include <stdio.h>

#define __STDC_CONSTANT_MACROS


extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/imgutils.h"
};
#include "SDL.h"


#pragma comment (lib, "avformat.lib")
#pragma comment (lib, "avutil.lib")
#pragma comment (lib, "avcodec.lib")
#pragma comment (lib, "swscale.lib")
#pragma comment (lib, "SDL2.lib")
#pragma comment (lib, "SDL2main.lib")


//Refresh Event
#define SFM_REFRESH_EVENT  (SDL_USEREVENT + 1)

#define SFM_BREAK_EVENT  (SDL_USEREVENT + 2)

int thread_exit=0;
int thread_pause=0;

int sfp_refresh_thread(void *opaque)
{
    thread_exit=0;
    thread_pause=0;

    while (!thread_exit) 
    {
        if(!thread_pause)
        {
            SDL_Event event;
            event.type = SFM_REFRESH_EVENT;
            SDL_PushEvent(&event);
        }
        SDL_Delay(40);
    }
    thread_exit=0;
    thread_pause=0;

    //Break
    SDL_Event event;
    event.type = SFM_BREAK_EVENT;
    SDL_PushEvent(&event);

    return 0;
}


void decode(AVCodecContext *pCodecCtx, AVPacket *packet, AVFrame *pFrame, AVFrame *pFrameYUV, SDL_Renderer* sdlRenderer, SDL_Texture* sdlTexture, struct SwsContext *img_convert_ctx)
{
    int ret = 0;
    int i = 0;
    static int cnt = 0;

    // 将原始包数据作为输入提供给解码器
    ret = avcodec_send_packet(pCodecCtx, packet);
    if (ret < 0) {
        fprintf(stderr, "Error sending a packet for decoding\n");
        return;
    }

    while (ret >= 0) {
        // 从解码器返回解码后输出数据
        ret = avcodec_receive_frame(pCodecCtx, pFrame);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
            break;
        } else if (ret < 0) {
            fprintf(stderr, "Error during decoding\n");
            break;
        }

        cnt++;

        sws_scale(img_convert_ctx, (const uint8_t* const*)pFrame->data, pFrame->linesize, 0, pCodecCtx->height, 
            pFrameYUV->data, pFrameYUV->linesize);

        //SDL---------------------------
        SDL_UpdateTexture( sdlTexture, NULL, pFrameYUV->data[0], pFrameYUV->linesize[0] );  
        SDL_RenderClear( sdlRenderer );  
        //SDL_RenderCopy( sdlRenderer, sdlTexture, &sdlRect, &sdlRect );  
        SDL_RenderCopy( sdlRenderer, sdlTexture, NULL, NULL);  
        SDL_RenderPresent( sdlRenderer );  
        //SDL End-----------------------

        printf("Succeed to decode %d frame!\r", cnt);
    }
}


int FindStartCode (unsigned char *Buf, int zeros_in_startcode)  
{  
    int info;  
    int i;  

    info = 1;  
    for (i = 0; i < zeros_in_startcode; i++) { 
        if(Buf[i] != 0) {  
            info = 0;
        }
    }

    if(Buf[i] != 1) { 
        info = 0; 
    }

    return info;  
}  


int getNextNal(FILE* inpf, unsigned char* Buf)//打开的文件句柄，和将读取的字节存储到Buf中  
{ 
    int pos = 0;  
    int StartCodeFound = 0;  
    int info2 = 0;  
    int info3 = 0;  
    int i = 0;  
    while (!feof(inpf) && (Buf[pos++] = fgetc(inpf)) == 0);//跳过NAL头  

    /*通过查找下一次的NAL头来计算本次的NAL的长度，根据长度来读取NAL数据包*/  
    while (!StartCodeFound)  
    {  
        if (feof (inpf))  
        {  
            return pos-1;  
        }  
        Buf[pos++] = fgetc (inpf);    
        info3 = FindStartCode(&Buf[pos-4], 3);  
        if(info3 != 1)  {
            info2 = FindStartCode(&Buf[pos-3], 2); 
        }
        StartCodeFound = (info2 == 1 || info3 == 1);  
    } 
    if (info2) {
        fseek (inpf, -3, SEEK_CUR); 
        return pos - 3;//返回的就是一个NAL的长度 
    } else {
        fseek (inpf, -4, SEEK_CUR); 
        return pos - 4;//返回的就是一个NAL的长度 
    } 
}



int main(int argc, char* argv[])
{
    FILE * inp_file; 
    int             videoindex = -1;
    AVCodecContext  *pCodecCtx;
    AVCodec         *pCodec;
    AVFrame *pFrame,*pFrameYUV;
    uint8_t *out_buffer;
    AVPacket *packet;
    //------------SDL----------------
    int screen_w,screen_h;
    SDL_Window *screen; 
    SDL_Renderer* sdlRenderer;
    SDL_Texture* sdlTexture;
    SDL_Rect sdlRect;
    SDL_Thread *video_tid;
    SDL_Event event;

    struct SwsContext *img_convert_ctx;

    /*分配内存，并初始化为0*/  
    unsigned char*Buf = (unsigned char*)calloc ( 500*1024, sizeof(char));  

    char filepath[]="D:\\Workspace\\7ffmpeg\\demo\\out\\out.h264";
    /*输入的文件*/  
    inp_file = fopen(filepath, "rb");  
    if (inp_file == NULL) {
        return -1;
    }


    /*查找 H264 CODEC*/  
    pCodec = avcodec_find_decoder(AV_CODEC_ID_H264);  
    if (pCodec == NULL) {
        return -1;
    } 

    /*初始化CODEC的默认参数*/  
    pCodecCtx = avcodec_alloc_context3(NULL); 
    if (pCodecCtx == NULL) {
        return -1;
    }
    pCodecCtx->width = 480;
    pCodecCtx->height = 800;
    pCodecCtx->pix_fmt = AV_PIX_FMT_YUV420P;


    //打开解码器
    if(avcodec_open2(pCodecCtx, pCodec, NULL)<0){
        printf("Could not open codec.\n");
        return -1;
    }

    /*为AVFrame申请空间，并清零*/  
    pFrame=av_frame_alloc();
    pFrameYUV=av_frame_alloc();
    if (pFrame==NULL || pFrameYUV==NULL) {
        printf("av_frame_alloc() failed!\n");
        return -1;
    }

    out_buffer=(uint8_t *)av_malloc(av_image_get_buffer_size(AV_PIX_FMT_YUV420P, pCodecCtx->width, pCodecCtx->height, 1));
    av_image_fill_arrays(pFrameYUV->data, pFrameYUV->linesize, out_buffer, AV_PIX_FMT_YUV420P, pCodecCtx->width, pCodecCtx->height, 1);

    img_convert_ctx = sws_getContext(pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt, 
        pCodecCtx->width, pCodecCtx->height, AV_PIX_FMT_YUV420P, SWS_BICUBIC, NULL, NULL, NULL); 


    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER)) {  
        printf( "Could not initialize SDL - %s\n", SDL_GetError()); 
        return -1;
    } 
    //SDL 2.0 Support for multiple windows
    screen_w = pCodecCtx->width;
    screen_h = pCodecCtx->height;
    screen = SDL_CreateWindow("ffmpeg player", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        screen_w, screen_h,SDL_WINDOW_OPENGL);
    if(!screen) {  
        printf("SDL: could not create window - exiting:%s\n",SDL_GetError());  
        return -1;
    }

    sdlRenderer = SDL_CreateRenderer(screen, -1, 0);  
    //IYUV: Y + U + V  (3 planes)
    //YV12: Y + V + U  (3 planes)
    sdlTexture = SDL_CreateTexture(sdlRenderer, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING,pCodecCtx->width,pCodecCtx->height);  

    sdlRect.x=0;
    sdlRect.y=0;
    sdlRect.w=screen_w;
    sdlRect.h=screen_h;

    packet=(AVPacket *)av_malloc(sizeof(AVPacket));

    video_tid = SDL_CreateThread(sfp_refresh_thread,NULL,NULL);
    //------------SDL End------------
    //Event Loop

    for (;;) {
        //Wait
        SDL_WaitEvent(&event);
        if(event.type==SFM_REFRESH_EVENT){

            if (!feof(inp_file)) {
                /*从码流中获得一个NAL包*/  
                int nalLen = getNextNal(inp_file, Buf);  

                AVPacket packet = {0};
                packet.data = Buf;
                packet.size = nalLen;

                decode(pCodecCtx, &packet, pFrame, pFrameYUV, sdlRenderer, sdlTexture, img_convert_ctx);

                av_packet_unref(&packet);
            } else{
                //Exit Thread
                decode(pCodecCtx, NULL, pFrame, pFrameYUV, sdlRenderer, sdlTexture, img_convert_ctx);
                printf("\n");
                thread_exit=1;
            }
        }else if(event.type==SDL_KEYDOWN){
            //Pause
            if(event.key.keysym.sym==SDLK_SPACE)
                thread_pause=!thread_pause;
        }else if(event.type==SDL_QUIT){
            thread_exit=1;
        }else if(event.type==SFM_BREAK_EVENT){
            break;
        }

    }
    

    sws_freeContext(img_convert_ctx);

    SDL_Quit();
    //--------------
    av_frame_free(&pFrameYUV);
    av_frame_free(&pFrame);
    avcodec_close(pCodecCtx);

    return 0;
}

```